---
title: "Brasserie"
description: "Une brasserie aux allures aléatoires ?"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/brasserie-banner.jpg"

tags:
  - "Bière"
  - "Levure"
  - "Cave"
---

Grâce aux talents créatifs de Matthieu, ses inspirations basées sur ses amis ou encore la mythologie nordique (Vikings), une dizaine de recettes ont été élaborées.

Pour l'instant la Ludovice, une bière sombre, une porter au goût et à l'amertume du chocolat noir, la Dax, une american brown ale douce et sucrée et la Yannis Pale Ale, une Swiss Pale Ale rafraichissante, un peu aléatoire et novatrice, sont disponibles à la dégustation.

N'hésitez pas à venir soutenir la brasserie sur place et déguster nos dernières créations.

Fini les bouteilles ! Nous avons décidé de nous lancé dans la laborieuse expérience des canettes. Après s'être rendu compte qu'il existe deux grandes familles de canette et que nous avions pas la bonne pièce. Nous avons pu nous lancer dans le réglage de la machine. Le résultat à l'air satisfaisant nous attendons encore de voir comment ça évolue car il semblerait qu'il ait quelques pertes.

Le grand retour de la Ludovice (se prononce à l'italienne, on est pas dans GTA Vice City Stories), une porter aux arômes de cacao amer, est en canette. Cette bière noire peu pétillante se dégustera parfaitement avec vos desserts. Température de service conseillé aux alentour de 12°C.

Plongez dans la légende de Freijdis Eirikrsdottir, la fille d'Eirikr Thorvaldson. Femme de caractère, on raconte que lors d'une expédition au Canada, elle aurait tué une dizaine d'islandais car ils l'aurait agacée. C'est pourquoi cette blonde est forte, puissante et risque de vous faire trébucher. Son délicat parfum saura-t-il vous envoûter ?

Certaines levures de fond de cuve post-fermentation sont récupérées pour les expériences de Yannis.

Rendez-vous sur https://brasserie-mfp.ch/

Venez aux events [Craft'n'fix]({{< ref "/actualites/soirees-craftnfix" >}}) pour déguster nos bières
