---
title: "Spirale aromatique"
description: "Constuire des structures réfléchies"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/projets-realises/spirale-aromatique/banner.jpg"

tags:
  - "Aromates"
  - "Épices"
  - "Médicinal"
  - "Culture"
  - "Cuisine"
---

La spirale aromatique est une structure qui profite de la disposition du soleil pour y faire pousser différentes plantes utilisées dans la cuisine et / ou en médecine. De part sa construction, certaines plantes vont bénéficier de plus de soleil que d'autres. Cela crée un petit écosystème où les plantes plus fragiles seront protegées du soleil par les autres plantes et / ou par la structure de la spirale et permet d'avoir en un seul endroit toutes les plantes pour préparer de bons plats.

A partir de tuiles de toit, nous avons élaboré et construit une spirale à aromates, basé sur les [modèles existants](https://lechamppanier.wordpress.com/une-visite-au-jardin/spirale-aromatique/).

Un [hôtel]({{< ref "/projets-en-cours/hotel-a-insectes" >}} "hôtel à insecte") spécialement aménagé avec des roseaux, bois troués et pives y sont intégrés.

{{< columns >}}
  {{< img
    src="/images/projets-realises/spirale-aromatique/spirale-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Yannis en train de prendre du bois"
    title="L'espace devant la porte d'entrée offre un environnement très ensoleillé."
  >}}
  {{< img
    src="/images/projets-realises/spirale-aromatique/spirale-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les rideaux dans les escaliers"
    title="Le pilier central permet de garder l'humidité et le frais de la spirale."
  >}}
  {{< img
    src="/images/projets-realises/spirale-aromatique/spirale-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Le jardin en hiver"
    title="Les tuiles sont utilisées pour créer la structure extérieure."
  >}}
  {{< img
    src="/images/projets-realises/spirale-aromatique/spirale-4.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un test de buttes"
    title="Des cailloux et de la terre remplissent l'antre de la spirale."
  >}}
  {{< img
    src="/images/projets-realises/spirale-aromatique/spirale-5.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un test de buttes"
    title="Toujours motivé, Matthieu remplit le centre de la spirale."
  >}}
  {{< img
    src="/images/projets-realises/spirale-aromatique/spirale-6.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un test de buttes"
    title="La petite spirale terminée, ses créateurs prennent le temps de l'admirer."
  >}}
{{< /columns >}}
