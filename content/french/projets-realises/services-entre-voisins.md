---
title: "Services entre voisins"
description: "Besoin d'aide pour un projet ? Donnons nous des coup de mains !"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/services-entre-voisins-banner.jpg"

tags:
  - "Aide"
  - "Communauté"
  - "Partage"
---

Besoin d'aide pour déménager un gros meuble ? Vous voulez construire un abri à vélo ? On est là !

Avec la motivation de chacun et un peu d'huile de coude, on peut réaliser de
grandes choses ! N'hésitez pas à [nous contacter]({{< ref "/a-propos/contact" >}}) et on pourra vous venir en aide ! Nous avons une liste d'outils que l'on peut prêter, disponibles sur la page [Matériel]({{< ref "/soutenir/materiel" >}}).

# Personnes de contact

En dehors de nous, les personnes suivantes sont là si nécessaire !

_Envoyez vos coordonnées si vous souhaitez apparaître dans cette liste !_

- _La liste est vide._
