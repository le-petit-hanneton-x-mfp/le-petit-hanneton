---
title: "\"Jette pas !\""
description: "Jette pas, et répare !"
kind: "page"

menu:
  main:
    parent: "projets-realises"

banner: "/images/jette-pas-banner.jpg"

tags:
  - "Réparation"
  - "Matériel électronique"
  - "Devis gratuit"

resources:
  - _target:
    name: "\"Jette pas !\" et viens faire réparer !"
    date: "20-28 avril 2019"
    url: "http://jette-pas.ch"
  - _target:
    name: "Nos incubés nous présentent leurs projets et livrent leurs premières impressions!"
    date: "15 avril 2019"
    author: "Impact Hub Lausanne"
    url: "https://lausanne.impacthub.net/nos-incubes-nous-presentent-leurs-projets-et-livrent-leurs-premieres-impressions/"
  - _target:
    name: "Impact Hub Lausanne"
    url: "https://lausanne.impacthub.net"
  - _target:
    name: "Circular Economy Transition"
    url: "https://www.cetransition.ch/fr/home"
  - _target:
    name: "L'événement Facebook avec quelques photos"
    date: "20-28 avril 2019"
    url: "https://www.facebook.com/events/314666385888330/?active_tab=discussion"
---

"Jette pas !" découle d'un précédent projet porté par Ludovic, qui a fortement influencé la philosophie du petit hanneton par la suite. Le concept est né d’un cours suivi à l'école d'ingénieur pour apprendre les bases de la création et de la gestion d’une entreprise. L'idée était de mettre en place des structures communautaires, sociales et conviviales permettant à tout un chacun de réparer et / ou faire réparer ses objets défectueux.

Ce projet a été sélectionné pour être présenté à Londres puis a gagné un autre concours pour participer au programme "[Circular Economy Transition](https://www.cetransition.ch/fr/home)". Ce programme national aide les projets s'inscrivant dans les notions du développement durable grâce à des structures d'accompagnement tel que [Impact Hub](https://impacthub.net/) et des coachs personnels.

Durant cette période, différents ateliers de réparation ont été organisés à Lausanne et Yverdon pour valider le concept. Ceux-ci ont reçu un très bon retour et ont fortement encouragé Ludovic à continuer dans cette direction. La situation de Ludovic permet à nouveau de remettre ce projet sur les rails, au travers du petit hanneton ! Pour plus d'informations sur l'histoire de "Jette pas !", différents liens sont disponibles à la fin de cette page !

{{< columns >}}
  {{< img
    src="/images/jette-pas-jette-pas-logo.png"
    column-classes="is-2 is-offset-5"
    figure-classes="is-1by1 m-0"
    alt="Logo de \"Jette pas !\""
  >}}
{{< /columns >}}

Le petit hanneton souhaite réduire le gaspillage des ressources en mettant en place des événements communautaires et sociaux permettant à tout un chacun de réparer et / ou faire réparer ses objets défectueux. En acceuillant les personnes dans un lieu chaleureux et convivial, nous espèrons que les personnes auront du plaisir à venir partager, échanger, aider et se faire aider dans nos locaux et partager les valeurs qui nous sont chères : réduire le gaspillage, changer la mentalité actuelle de jeter plutôt que de réparer, remettre l'humain au centre et mettre en place un concept éthique, viable, durable et solidaire.

Dans un premier temps, il s'agit de réparer des objets électroniques (ordinateurs et téléphones) simplement parce que l'on a les compétences pour le faire. Puis, au fur et à mesure que l'initiative prend de l'ampleur, pouvoir proposer la réparation de tout type d'objets.

Le petit hanneton se réjouit de t'accueillir pour réparer tes objets et partager avec toi autour d'un café prochainement !

_Il n'y a pas d'événements officiels pour le moment mais n'hésitez pas à passer de façon spontanée et on pourra sans doute vous aider !_

