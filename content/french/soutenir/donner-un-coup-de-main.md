---
title: "Donner un coup de main"
description: "Venez aider le petit hanneton !"
kind: "page"

menu:
  main:
    parent: "soutenir"

banner: "/images/donner-un-coup-de-main-banner.jpg"
---

Le petit hanneton a toujours besoin d'un coup de main ! Qu'il s'agisse de construire quelque chose, s'occuper du jardin et de la maison ou encore de finir un fût, des mains supplémentaires sont toujours les bienvenues !

N'hésitez pas à regarder les [Projets en cours]({{< ref "/projets-en-cours" >}} "Projets en cours") et utiliser les différents moyens de [Contact]({{< ref "/a-propos/contact" >}} "Contact") si vous voulez nous aider !
