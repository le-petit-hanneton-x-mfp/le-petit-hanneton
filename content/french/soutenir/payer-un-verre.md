---
title: "Payer un verre"
description: "Aidez à faire grandir le petit hanneton"
kind: "page"

menu:
  main:
    parent: "soutenir"

banner: "/images/payer-un-verre-banner.jpg"
---

Le petit hanneton n'est pas (encore ?) une association au sens juridique du terme. Il s'agit bien d'un projet mené par trois jeunes durant leur temps libre. La notion d'argent n'a pas grandement sa place dans leurs valeurs, préférant l'entraide et les échanges de services. Néanmoins si vous le souhaitez, vous pouvour soutenir financièrement le projet.

Vos dons permettront de pouvoir acquérir le [Matériel]({{< ref "/soutenir/materiel" >}} "Matériel") nécessaire à la réalisation des activités du petit hanneton et réaliser ces dernières !

Vous pouvez préciser dans l'objet de votre virement pour quel projet vous souhaitez mettre de l'argent et si vous souhaitez ou non apparaître dans la liste des [Remerciements]({{< ref "/a-propos/remerciements" >}} "Remerciements"). Vous receverez toute la gratitude de la part de nos trois cons pairs !

_Matthieu Joly et consorts - CH15 0076 8300 1586 8160 8_
