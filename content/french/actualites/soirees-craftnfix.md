---
title: "Soirées Craft'n'fix été 2023"
description: "Viens déguster des bières et passer un bon moment !"
author: "Brasserie MFP x Le petit hanneton"
kind: "page"
draft: false
date: 2023-05-04T20:00:00+02:00
banner: "/images/actualites/craftnfix-2023/banner.jpg"
---

On a un plan de fou pour des vendredis soir cet été ! Si t'as rien de prévu, viens nous rejoindre à nos évènements "Craft'n'Fix" sur notre terrasse/jardin. La première édition sera le 16 juin dès 16h, puis toutes les 3 semaines jusqu'en octobre (sous réserve de modifications).

Les dates :
- 16 juin
- 7 juillet
- 28 juillet
- 18 août
- 8 septembre
- 29 septembre

Au programme :
- Dégustation de bières artisanales que l'on brasse avec amour, avec 3 nouvelles bières à déguster à chaque fois
- Un grill à disposition pour qu'on puisse se régaler et snacks canadiens à partager. Ça promet d'être une explosion de saveurs et de découvertes culinaires
- Réparation d'objets où tu pourras apporter tes affaires à réparer ou bien partager tes compétences pour aider les autres à la manière d'un repair café
- Peut être des mini-jeux dans le jardin sous forme de joutes (ça va finir au lac ct'histoire)

Il est préférable d'annoncer à l'avance le type d'objet que tu aimerais réparer. Contacte-nous pour que l'on puisse préparer le nécessaire et t'assurer une expérience optimale.

Et avant de partir, n'oublie pas de faire un tour dans notre cabanon à troc. Qui sait, tu pourrais y trouver des trésors cachés et faire de belles découvertes !

Il y aura également une crousille sur place pour soutenir la brasserie et continuer à créer des bières inédites.

Si t'es partant(e), ramène tes potes, ta bonne humeur et un plat à partager, et rejoins-nous pour une soirée cool et conviviale. Pas besoin de s'inscrire, mais ça nous aiderait à mieux préparer l'évènement et à garantir que tout le monde passe une super soirée.

Viens boire une bière, manger un burger, réparer ton vélo ou ton grille-pain et soutenir notre brasserie, tout ça en bonne compagnie. C'est l'occasion idéale pour boire un coup entre potes, se détendre et profiter de la fin de la semaine.

On espère te voir bientôt !

Bisous

https://brasserie-mfp.ch/fr/blog/soiree-craft-n-fix/

Restez au courant des dernières infos sur les soirées Craft'n'fix sur Telegram
<div class="container">
    <div class="content">
    <span class="icon ml-3 mr-3"><a href="https://t.me/+uqlHfL2PXD04ZjU0"><i
                class="fab fa-3x fa-telegram"></i></a></span>
    </div>
</div>
