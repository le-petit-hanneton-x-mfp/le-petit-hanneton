---
title: "Récapitulatif 2022"
description: "Une année tranquille et pleine de bonnes surprises"
author: "Le petit hanneton"
kind: "page"
draft: false
date: 2022-12-30T20:00:00+02:00
banner: "/images/actualites/recapitulatif-2022/banner.jpg"
---

Malgré une annonce de vouloir vous proposer des actualités plus courtes, mais plus régulières, celles-ci n'ont pas vu le jour. En effet, il faut compter quelques heures pour chaque article rédigé sur ce petit blog. Entre la rédaction du texte, le choix et la préparation des médias (photos et vidéos), la relecture et les corrections des fautes d'orthographe, les activités de chacun, on se rend compte que la dernière publication date déjà d'il y a une année..!

Heureusement, il s'agit pour nous d'une année positive, remplie de chouettes moments et toujours avec la motivation de montrer que l'on peut vivre autrement que l'on souhaite vous partager aujourd'hui !

Voici le récapitulatif de cette année 2022, qui arrive déjà à sa fin.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-07-1217.jpg"
    date="2022-01-07 12:17"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La maison et le jardin sous la neige"
    title="La maison sous la neige au mois de janvier."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-27-1048-2.jpg"
    date="2022-08-27 10:48"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une fleur devant l'étang"
    title="L'étang a été un élément phare de notre jardin cette année."
  >}}
{{< /columns >}}

# Janvier

Le premier mois de l'année a permis à nos compères de reprendre le rythme après les fêtes.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-14-1158.jpg"
    date="2022-01-14 11:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Des oiseaux dans le lac"
    title="Les oiseaux dans le lac qui ne semblent pas être perturbés par le froid."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-14-1200.jpg"
    date="2022-01-14 12:00"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Des branches gelées"
    title="Le froid et la glace créent de très belles structures dans la Grande Cariçaie."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-01-14-0817.mp4"
    date="2022-01-14 08:17"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le lac gêlé"
    title="Yannis profite du bord du lac gêlé. Aucun bâteau ne pourrait passer."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-19-1130.jpg"
    date="2022-01-19 11:30"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia dans son panier"
    title="Nalia se repose confortablement dans son panier."
  >}}
{{< /columns >}}

## Réserve de bois 

Le bois stocké sur la terrasse a été utilisé dans son entièreté et nous avons entamé le bois du bûcher officiel. Le froid n'est plus un problème, sans doute nous nous y sommes habitués ! En moyenne, il doit faire 17°C dans la maison, habituellement avec un 12-13°C au matin. Après un week-end où personne n'était à la maison, un 9°C a été enregistré ! Il serait intéressant d'avoir des mesures quotidiennes, peut-être un projet à venir...

## Les esprits du jardin

Depuis quelque temps, des phénomènes étranges se passent dans notre jardin. Des objets déplacés, des bruits durant la nuit ou encore des caisses qui s'envolent avec le vent, c'est un mystère de notre jardin qui nous livre des surprises pour tout le reste de l'année..!

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-05-1209.jpg"
    date="2022-01-05 12:09"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une caisse dans le jardin"
    title="Une caisse qui s'est envolée..?"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-05-1806.jpg"
    date="2022-01-05 18:06"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une planche tombée de la facade"
    title="Une planche qui tombe de la facade..?"
  >}}
{{< /columns >}}

## Souris

De nombreuses souris (certaines plus dodues que d'autres) ont été capturées à la cave ; nous avons arrêté de compter après la 6ème. On doit être autour de la quinzaine à ce jour. 

Presque toutes ont pu être libérées dans la Grande Cariçaie où on espère qu'elles y trouveront le même confort que dans notre maison. Seule une n'a pas survécu dans sa cage. En effet, bien que l'on regarde régulièrement pour ne pas les laisser mourir de faim, en une nuit, elle avait commencé à ronger le bois de la cage, faisant un tas de sciure autour de celle-ci. Lorsque nous l'avons retrouvée, elle était déjà morte. Est-ce que le bois est traité pour éviter que les souris ne puissent s'échapper en mangeant leur cage..? Aussi, n'ayant plus de fromage, certaines ont eu le luxe de pouvoir profiter de morceaux de saucisson pour patienter leur libération.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-09-2009.jpg"
    date="2022-01-09 20:09"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une souris dans sa cage"
    title="Une petite souris toute mignonne."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-01-11-0808.jpg"
    date="2022-01-11 08:08"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une autre souris dans sa cage"
    title="Une autre petite souris toute mignonne."
  >}}
{{< /columns >}}

## La brasserie

La brasserie se professionnalise ! Matthieu et Yannis mettent les bouchées doubles pour vous fournir un maximum de variété dans les mois qui viennent. Un logo ainsi que des étiquettes pour les canettes devraient voir le jour tout prochainement grâce à l'aide de Mathilde, Jess et Audrey. 

## Immersion dans le lac

Lors des fêtes, une proposition (et seulement une proposition) de résolution de la nouvelle année a été proposée par Ludo. Celle d'aller se baigner dans le lac chaque jour de l'année 2022. Ceux qui connaissent Ludo savent la démesure de la proposition. Celui qui pensait émettre une blague a été pris très au sérieux par les colocs et par Mathilde. De nombreuses tentatives de chantage ont été faites pour essayer de nous encourager à le faire. Au final, au grand bonheur de Ludo, les oppressions ont cessé et, pour le moment, le grand Yannis est l'unique à avoir osé mettre les pieds dans le lac. En dehors de la vidéo qui suit, nous n'avons aucune preuve qu'il ait réellement été nagé mais nous avons confiance en sa parole.

Matthieu et Ludo savent qu'elles sont les vraies priorités.

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-01-30-1830.mp4"
    date="2022-01-30 18:30"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis qui va se baigner dans le lac"
    title="Nous ne saurons jamais si Yannis est allé jusqu'au bout de sa baignade ou non mais respect à lui."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-01-30-1909.mp4"
    date="2022-01-30 19:09"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une raclette qui s'emflamme"
    title="Pendant ce temps, Matthieu et Ludo mangent la raclette flambée à l'abricotine chez Miguel."
  >}}
{{< /columns >}}

# Février

Un mois à nouveau très tranquille dans la maison du petit hanneton. Les soirées séries au bord du feu s'enchaînent et le temps passe gentiment.

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-02-09-1758-1.mp4"
    date="2022-02-09 17:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de Matthieu qui promène Nalia"
    title="Matthieu tente de partager sa promenade avec Nalia."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-02-09-1758-2.mp4"
    date="2022-02-09 17:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de Matthieu qui promène Nalia"
    title="Nalia semble heureuse de se promener avec Matthieu."
  >}}
{{< /columns >}}

## Préparation du jardin

Le jardin est préparé petit à petit. Certains arbres sont taillés, les dernières feuilles mortes ramassées. 

La barrière en bois qui donne sur la route est maintenue par de gros pots de fleurs et d'autres barrières grillagées mises en place le long des haies par Yannis et Ludo devraient permettre de laisser Nalia vagabonder dans le jardin à sa guise d'ici peu. 

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-02-10-1533-1.jpg"
    date="2022-02-10 15:33"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La barrière (ouverte) au fond du jardin"
    title="La barrière (ouverte) devrait permettre à Nalia de se balader dans le jardin sans souci."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-02-10-1533-2.jpg"
    date="2022-02-10 15:33"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La barrière (fermée) au fond du jardin"
    title="La barrière (fermée) devrait permettre à Nalia de se balader dans le jardin sans souci."
  >}}
{{< /columns >}}

## Fête d'anniversaire de Matthieu

Matthieu a souhaité organiser son anniversaire (c'est le 16 février si besoin, il attend les cartes pour l'année prochaine) mais c'était sans compter sur le COVID que Yannis et Matthieu auront fini par attraper.

La fête est annulée mais sera reportée plus tard dans l'année (plus à ce sujet un peu plus tard).

## Exil

Après beaucoup de questionnement, un aménagement du fourgon pour Yannis digne des plus grands hôtels avec cuisinière, gâteau livré au fourgon, Internet, console et manette de jeux, Ludo ira habiter quelque temps chez Mathilde puis chez David afin de laisser la maison à Matthieu et Yannis. Mathilde finira elle aussi par l'attraper et Ludo arrive encore à y échapper (du moins à sa connaissance).

Tout le monde se remet bien et c'est un retour à la tranquillité que se termine le mois de février.

## Virée en moto de Matthieu

Matthieu commence à maîtriser la moto et profite des premiers jours de beau pour s'entraîner avec un joli tour à moto.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-02-28-1403.jpg"
    date="2022-02-28 14:03"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La vue sur le lac du tour à moto de Matthieu"
    title="La vue sur le lac du tour à moto de Matthieu en milieu de journée."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-02-28-1708.jpg"
    date="2022-02-28 17:08"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La vue sur le lac du tour à moto de Matthieu"
    title="La vue sur le lac du tour à moto de Matthieu en fin de journée."
  >}}
{{< /columns >}}

Le mois de mars s'annonce avec plus d'activités autour de la maison et les premiers semis devraient être préparés lorsqu'il fera un peu plus chaud.

# Mars

Les prémices du printemps arrivent !

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-04-0910.jpg"
    date="2022-03-04 09:10"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia assise à table"
    title="Nalia a l'honneur de pouvoir manger avec nous, parfois."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-0844.jpg"
    date="2022-03-26 08:44"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le prunier avec des bourgeons"
    title="Notre vieux prunier se réveille avec l'arrivée du printemps."
  >}}
{{< /columns >}}

## L'étang

L'étang continue d'être aménagé par Yannis et les plans du jardin sont préparés mentalement pour attaquer la nouvelle saison qui s'annonce.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-02-1405.jpg"
    date="2022-03-02 14:05"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'étang au début du printemps"
    title="Des cailloux ont été aménagés au bord de l'étang pour les batraciens"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-24-1714.jpg"
    date="2022-03-24 17:14"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'étang au début du printemps"
    title="Des cailloux ont été aménagés au bord de l'étang pour les batraciens"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-24-1726.jpg"
    date="2022-03-24 17:26"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'étang au début du printemps"
    title="Arriverez-vous à compter le nombre de grenouilles présentes sur cette photo ?"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-24-1731.jpg"
    date="2022-03-24 17:31"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="L'étang au début du printemps"
    title="Arriverez-vous à compter le nombre de grenouilles présentes sur cette photo ?"
  >}}
{{< /columns >}}

## Début du nouveau travail de Matthieu

Matthieu débute un nouveau travail à Fribourg et s'y trouve très bien ! Il s'agit d'un grand changement au niveau du temps de trajet (1h pour aller à Fribourg vs. 2h30 pour aller jusqu'à Bulle) et l'environnement dynamique lui correspond parfaitement.

## Serre de récupération

Le début du mois a vu la construction d'une serre fabriquée à partir d'éléments de récupération. Une voisine nous a donné plusieurs fenêtres et planches qui nous ont permis de réaliser une grande serre à salades. Celles-ci devraient être un peu plus protégées des limaces et la température monte facilement à 20°C.

Une seconde serre pour les semis pourrait voir prochainement le jour avec une seconde fenêtre plus petite.

Une autre construction permet de protéger les plantes fragiles des intempéries et de la grêle tout en permettant une température ambiante normale.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-04-1500.jpg"
    date="2022-03-04 15:00"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le matériel pour fabriquer les serres"
    title="Nous fabriquons les serres sous la surveillance de la responsable de chantier Nalia."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-04-1631-1.jpg"
    date="2022-03-04 16:31"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La fenêtre pour la serre à salade"
    title="La première fenêtre servira pour fabriquer la serre à salades."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-04-1631-2.jpg"
    date="2022-03-04 16:31"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le premier abri pour les buttes de permaculture"
    title="La seconde fenêtre permettra de protéger les semis sur les buttes de permaculture."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-04-1631-3.jpg"
    date="2022-03-04 16:31"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le jardin avec les deux serres"
    title="La terrasse est notre place de travail habituel pour ce genre de bricolages."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-22-1449.jpg"
    date="2022-03-22 14:49"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis plante les graines dans la serre"
    title="Yannis plante les graines dans la serre."
  >}}
{{< /columns >}}

## Identité visuelle de la brasserie 

Le même week-end a vu la réunion de Mathilde, Jess et Audrey pour la réalisation de l'identité visuelle de la brasserie MFP. Après une présentation des réalisations de Matthieu et plusieurs sketchs de la part des cheffes graphistes, un logo a été imaginé ! On se réjouit de voir la suite.

## Mort de notre sonnette

La sonnette sans fil de notre maison a décidé de rendre l'âme. Une nuit, elle a commencé à sonner à 4h14, avec des mélodies aléatoires de son registre.

## COVID, le retour

C'est finalement (et enfin) au tour de Ludo d'avoir le COVID, virus auquel il avait réussi à échapper jusque-là. Cloué au lit pendant une semaine, il s'en remettra plutôt bien.

## Les bons repas de la coloc

Le grand chef (_"chef, oui chef !"_) Matthieu prépare toujours ses bons plats dont Yannis et Ludo se régalent.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-23-1925-1.jpg"
    date="2022-03-23 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="De l'avocat dans une poêle"
    title="Matthieu lie les meilleurs ingrédients dans ses repas."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-23-1925-2.jpg"
    date="2022-03-23 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Matthieu aux fourneaux"
    title="Matthieu aux fournaux en train de servir son plat."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-23-1925-3.jpg"
    date="2022-03-23 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="De l'avocat dans une poêle"
    title="Le chef nous concocte toujours d'excellents repas."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-23-1925-4.jpg"
    date="2022-03-23 19:25"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="De l'avocat dans une poêle"
    title="Les repas de Matthieu sont toujours très bons !"
  >}}
{{< /columns >}}

## Le rattrapage de l'anniversaire de Matthieu, aka "Suze Party"

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-1358-1.jpg"
    date="2022-03-26 13:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis et Ludo sur les chaises longues"
    title="Le soleil permet déjà de se prélasser en attendant les invités."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-1358-2.jpg"
    date="2022-03-26 13:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia dans l'herbe"
    title="Nalia reste souvent au jardin pour y bronzer."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-2110.jpg"
    date="2022-03-26 21:10"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Mathilde et Ludo dans leur costume Suze"
    title="Mathilde et Ludo dans leur costume Suze."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-2215-1.jpg"
    date="2022-03-26 22:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Mattieu dans son costume Suze"
    title="Matthieu dans son costume Suze."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-2215-2.jpg"
    date="2022-03-26 22:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La bande dans leur costume Suze"
    title="La bande dans leur costume Suze."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-2215-3.jpg"
    date="2022-03-26 22:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La bande dans leur costume Suze (derrière)"
    title="La bande dans leur costume Suze (derrière)."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-03-26-2215-4.jpg"
    date="2022-03-26 22:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La bande dans leur costume Suze"
    title="Miguel est très content de son costume et de la soirée."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-03-26-2215.mp4"
    date="2022-11-08 22:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vidéo de la soirée"
    title="Matthieu n'a aucun problème pour s'asseoir avec son accoutrement."
  >}}
{{< /columns >}}

# Avril

Une petite vague de froid s'abat sur la colocation, mais les jours ensoleillés reviennent rapidement.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-01-1743.jpg"
    date="2022-04-01 17:43"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le jardin sous une petite couche de neige"
    title="Le retour de la neige sur le jardin pour rappeler qu'elle n'est pas encore partie."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-10-1515.jpg"
    date="2022-04-10 15:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia qui dort au soleil"
    title="Quelques jours plus tard, le retour du soleil permet à Nalia de se détendre."
  >}}
{{< /columns >}}

## La vie du jardin

Les bourgeons ont commencé à sortir et, avec eux, le retour de la neige donne un contraste intéressant.

La nature se réveille au fur à mesure des jours de beau et les premières fleurs de l'étang ne tardent pas à fleurir. Les grenouilles et crapauds s'y trouvent très bien et cohabitent avec elles. L'écosystème est riche et profite à plein d'espèces. Les oiseaux viennent s'y baigner, les insectes virevoltent autour des plantes et les grenouilles arrivent, parfois, à en attraper pour se nourrir.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-02-0740.jpg"
    date="2022-04-02 07:40"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les bourgeons du prunier sous la neige"
    title="La neige est revenue alors que le prunier se réveillait."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-02-0742.jpg"
    date="2022-04-02 07:42"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une vue sur l'étang avec la neige"
    title="Les plantes aquatiques supportent bien le retour du froid."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-02-0744.jpg"
    date="2022-04-02 07:44"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le magnolia sous la neige"
    title="Le magnolia a eu le temps de faire ses fleurs avant la neige."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-11-1209.jpg"
    date="2022-04-11 12:09"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une fleur de prunier"
    title="Malgré l'âge de notre prunier, il arrive encore à faire de belles fleurs."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-14-1213.jpg"
    date="2022-04-14 12:13"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le jardin depuis la terrasse"
    title="Le jardin se remet de la neige et du froid."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-15-1217.jpg"
    date="2022-04-15 12:17"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une plante aquatique en fleurs"
    title="Les plantes aquatiques ont donné leurs premières fleurs."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-22-1448.jpg"
    date="2022-04-22 14:48"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les grenouilles au bord de l'étang"
    title="Au moindre rayon de soleil, les grenouilles reviennent sur le bord de l'étang."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-22-1801.jpg"
    date="2022-04-22 18:01"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis qui porte Nalia"
    title="Nalia est très affectueuse et se laisse facilement porter (si on arrive à la soulever)."
  >}}
{{< /columns >}}

## Les premiers trèfles à quatre feuilles

Du trèfle a poussé naturellement dans un des bacs que nous avions. Il s'avère que celui-ci a donné des trèfles à quatre feuilles tout le long de la saison ! Un futur projet d'attraction pour les touristes devrait leur permettre de trouver à coup sûr un trèfle à quatre feuilles qui pourra être pris en photo. _"Venez trouver la chance au petit hanneton !"_

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-04-1713.jpg"
    date="2022-04-04 17:13"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un trèfle à quatre feuilles"
    title="Au jardin, une botte de trèfle donne régulièrement des trèfles à quatre feuilles."
  >}}
{{< /columns >}}

## Préparation des buttes et premiers semis

Le retour du soleil nous encourage à préparer le jardin avec les feuilles mortes stockées ces derniers mois dans un des bidons qu'on avait. Cette année, les semis semblent plus vigoureux mais nous achèterons aussi quelques plantons à côté.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-04-1555.jpg"
    date="2022-04-04 15:55"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Vue sur le jardin avec les buttes préparées"
    title="Les feuilles sont mises sur les buttes pour le reste de l'année."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-04-1711.jpg"
    date="2022-04-04 17:11"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Buttes recouvertes de feuilles"
    title="Les feuilles mortes continueront à se décomposer."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-04-1712.jpg"
    date="2022-04-04 17:12"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Buttes préparées"
    title="Cette technique permet de nourrir le sol et ce, sans abîmer les couches de la terre."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-14-0637.jpg"
    date="2022-04-14 06:37"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Plants de tomates"
    title="On espère que les plants de tomates auront plus de succès que l'année passée."
  >}}
{{< /columns >}}

## Aménagement de l'avant de la maison

Le devant de la maison est aussi aménagé avec un test de la part de Ludo. Il y mettra plusieurs graines de diverses variétés de plantes et _"celles qui donneront quelque chose donneront quelque chose"_. Des courgettes, des citrouilles, du maïs et pleins d'autres seront mises en terre. Cette "technique" a eu plus ou moins de succès.

Un jour nous auront à nouveau une belle palissade de plantes sur ce côté de la maison.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-26-1420-1.jpg"
    date="2022-04-26 14:20"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Spirale aromatique"
    title="Quelques plantes aromatiques colonisent la spirale"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-26-1420-2.jpg"
    date="2022-04-26 14:20"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Zone à plantes grimpantes"
    title="La zone à plantes grimpantes servira à différents légumes cette année"
  >}}
{{< /columns >}}

## Nalia dans l'histoire du Simplon

Lors de vacances à l'hospice du Simplon avec Mathilde, Ludo a trouvé les premières traces de Nalia dans l'histoire du Simplon. Ce magnifique portrait d'un personnage inconnu est accompagné d'une ancêtre à Nalia, comme l'image le montre bien. Les origines de la saucisse ne peut pas s'y méprendre.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-22-1235.jpg"
    date="2022-04-22 12:35"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un portrait peint avec une ancêtre à Nalia"
    title="La bête menaçante ressemble étrangement à Nalia."
  >}}
{{< /columns >}}

## Le dernier feu de la saison ?

Selon la règle de l'hiver 2021-2022, un feu ne peut être effectué que lorsqu'il fait moins de 15°C dans la maison. Le 26 avril, 19h38, le thermomètre nous autorise à le faire, mais les souvenirs ne permettent plus de savoir s'il a bien été effectué ou non. On arrive petit à petit sur le beau, peut-être qu'il s'agissait du dernier feu de la saison !

Nous rassurons nos lecteur-trice-s que nous faisons des feux si nécessaire lorsque nous avons des invité-e-s !

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-26-1938.jpg"
    date="2022-04-26 19:38"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Thermomètre affichant 14.3° et 61% d'humidité RH"
    title="Il ne fait pas tant chaud chez nous (à l'intérieur oui oui)."
  >}}
{{< /columns >}}

## Les premiers animaux dans le jardin

Le printemps annonce le retour des animaux dans le jardin. Les hérissons sont nombreux et eux-aussi bien dodus.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-04-27-0812.jpg"
    date="2022-04-27 08:12"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Petite souris"
    title="La petite souris cherche nos dents (on les as entérées comme engrais)."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-04-26-2301.mp4"
    date="2022-04-26 23:01"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia renifle un hérisson"
    title="On espère que les hérissons mangeront les limaces."
  >}}
{{< /columns >}}

# Mai

Le mois de mai annonce les premières pousses dans le jardin et la mise en terre des semis.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-06-1643-1.jpg"
    date="2022-05-06 16:43"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin prêt pour la saison"
    title="Le jardin est prêt pour le début de saison."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-09-1709.jpg"
    date="2022-05-09 17:09"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin ensoléillé"
    title="Les premières semences ont été plantées après les Saints de glaces."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-26-1413.jpg"
    date="2022-05-26 14:13"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Premières pousses"
    title="Les premières pousses pointent déjà le bout de leur nez."
  >}}
{{< /columns >}}

## Coup de chaud dans la serre

La magnifique serre a été très efficace pour les petites salades. Ouverte la journée, fermée la nuit, elle protège nos plants des limaces. Malheureusement, un matin, nous avons oublié de l'ouvrir... A midi, toutes les plantes étaient mortes...

Nous avons tenté de remettre des plants mais le découragement général suite à cet échec ainsi que le nombre de limaces ont eu raison de la serre pour cette saison. Les mauvaises herbes ont repris leurs droits. Nous espérons faire mieux l'année prochaine !

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-04-1700.jpg"
    date="2022-05-04 17:00"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Plantes brûlées dans serre"
    title="La serre est restée fermée un jour de grand soleil, brûlant tout son contenu."
  >}}
{{< /columns >}}

## Déracinement des buissons

Yannis aimant manier de gros outils, il s'occupe de déraciner les plantes et arbustes que l'on avait coupés.

La nouvelle place devrait permettre d'y mettre des mûres, des framboises et des fraises pour la saison prochaine. Nous souhaitons faire un petit coin à baies relativement à l'abri du soleil et facilement accessible.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-06-1644-1.jpg"
    date="2022-05-06 16:44"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Plante déracinée"
    title="Les racines sont nombreuses, la sueur coule."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-06-1644-3.jpg"
    date="2022-05-06 16:44"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Plante déracinée"
    title="Quelques coups de pioches plus tard."
  >}}
{{< /columns >}}

## Petits pois

Les petits pois ont survécu à leurs premiers jours et donneront lieu à un ou deux plats très bons plus tard dans la saison. 

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-07-0650.jpg"
    date="2022-05-07 06:50"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Pousse de pois"
    title="Les premiers pois pointent le bout de leurs feuilles."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-05-02-2200.mp4"
    date="2022-05-02 22:00"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Hérisson"
    title="Les hérissons intriguent Nalia qui ne sait jamais quoi faire."
  >}}
{{< /columns >}}

## Passage à 60% de Ludo

Le mois de mai annonce le passage à 60% de Ludo au lieu des 80% qu'il avait précédemment. Il espère ainsi avoir plus de temps pour ses projets personnels et pour la maison.

## Les abeilles charpentière profitent de notre bucher

Quelques abeilles charpentières sortent régulièrement du bûcher. Nous n'avons pas de photos à vous montrer mais il s'agit de très belles abeilles noires et bleues avec un corps immense dont vous trouverez des informations sur le site suivant: [_L'abeille charpentière (Xylocopa violacea)_ - totholz.wsl.ch](https://totholz.wsl.ch/fr/portraits-despeces/l-abeille-charpentiere.html).

Nous avons appris notamment qu'il s'agit d'une espèce en voie de disparition et nous sommes heureux qu'elles aient trouvé refuge dans notre jardin. On espère les voir annuellement !

## Les plantes de l'étang utilisent l'espace

Les nénuphars et autres plantes aquatiques commencent à prendre l'espace qui leur ait dédié. Leur pousse est impressionnante et recouvrira bientôt l'intégralité de la surface de l'eau, donnant lieu à de nombreuses cachettes aux grenouilles qui iront s'y cacher lorsque nous passons à côté pour promener Nalia.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-10-1930.jpg"
    date="2022-05-10 19:30"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Plantes de berge étang"
    title="Les plantes aquatiques transplantées en octobre ont survécu et commencent à pousser."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-18-1734.jpg"
    date="2022-05-18 17:34"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Iris"
    title="Notamment de très belles Iris qui ont des couleurs magnifiques et embelissent encore plus l'étang."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-18-1937.jpg"
    date="2022-05-18 19:37"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Iris et autres"
    title="Les autres semblent aussi s'y plaire."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-28-1204.jpg"
    date="2022-05-28 12:04"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Grenouilles"
    title="Les grenouilles se prélassent volontiers au soleil."
  >}}
{{< /columns >}}

## Installation d'un bidon de refroidissement pour la bière

A l'aide d'une pompe, l'eau du bidon sera utilisée en circuit fermé pour refroidir le moût à la fin du brassage. Ceci devrait permettre d'économiser de l'eau pour la fabrication de la bière.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-22-1258-1.jpg"
    date="2022-05-22 12:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Bidon refroidissement bière"
    title="Situé devant la porte de la cave pour un accès facilité, il s'agit d'une réserve importante d'eau fraîche."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-22-1258-2.jpg"
    date="2022-05-22 12:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Bidon refroidissement bière"
    title="Trônant sur son socle, le roi bidon a permis d'économiser plusieurs centaines de litres d'eau cette année."
  >}}
{{< /columns >}}

## Récupération d'eau de pluie par la chenau

Nous avons un bassin à l'avant de la maison. Cependant, celui n'avait plus à la base de tube pour retenir l'eau et son remplissage aurait nécessité de le remplir avec l'eau courante. Nous avons installé un collecteur d'eau de pluie qui prend l'eau directement depuis l'intérieur de la chenau.

Après un coup de perceuse, celui-ci est installé et il ne reste plus qu'à attendre la première pluie pour voir son efficacité.

Une crainte que nous avions était les moustiques. Heureusement, les larves n'ont pas trop proliféré cet été.

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-05-23-0959.mp4"
    date="2022-05-23 09:59"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Déviation de l'eau de la chenau"
    title="Ludo se réjouissait de voir le résultat de son installation sur la chenau avec la première pluie."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-05-23-1003.mp4"
    date="2022-05-02 10:03"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Déviation de l'eau de la chenau"
    title="Enfin, l'eau du toit permet de remplir le bassin. Il permettra d'arroser le devant de la maison."
  >}}
{{< /columns >}}

# Juin

Nous rentrons dans les mois beaux et chauds. La nature est en plein essor et le jardin se métamorphose de jour en jour.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-03-1355.jpg"
    date="2022-06-03 13:55"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Couple de cygnes et leurs bébés"
    title="Les cygnes protègent férocement leurs petits, surtout de Nalia."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-03-1811.jpg"
    date="2022-06-03 18:11"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia sur la terrasse"
    title="Nalia, ses fameuses positions et expressions de chien triste."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1456-3.jpg"
    date="2022-06-10 14:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin mi-juin"
    title="Le printemps est bien installé, tout pousse à son rythme."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-25-1528.jpg"
    date="2022-06-25 15:28"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Riz filet mignon sauce Matthieu"
    title="Matthieu nous régale encore de ses créations culinaires."
  >}}
{{< /columns >}}

## Attaques des limaces 2 bis, le retour du remake

Malheureusement, les limaces profitent plus de nos plantes que nous pour la seconde année consécutive... Certaines photos datent du mois précédent mais nous avons décidé de les mettre toutes en un bloc afin que la déprime soit concentrée à un seul endroit de cette actualité.

Nous avons quelques idées pour l'année prochaine afin de combattre ces bêtes sans les tuer. Nous verrons si ces stratégies seront suffisantes pour enfin manger nos récoltes.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-17-0637-1.jpg"
    date="2022-05-17 06:37"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Limace"
    title="..."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-05-17-0637-2.jpg"
    date="2022-05-17 06:37"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Limace"
    title="..."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-01-2219.jpg"
    date="2022-06-01 22:19"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Limaces"
    title="..."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-25-2047.jpg"
    date="2022-06-25-2047"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Limaces"
    title="..."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-29-0615.jpg"
    date="2022-06-29 06:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Limaces"
    title="\*déprime\*"
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-06-28-0813.mp4"
    date="2022-06-28 08:13"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Pot dans véranda"
    title="Même les escargots tentent d'accéder."
  >}}
{{< /columns >}}

## Culture de trèfles à quatre feuilles

Comme évoqué plus tôt, nous avons réussi à cultiver une botte de trèfle qui donne de très jolis et particuliers trèfles à quatre feuilles. Nous espérons que celle-ci survivra pour les années futures et, pourquoi pas, se lancer dans la sélection génétique pour battre le record du monde. Le record actuel est un trèfle à 56 (!) feuilles !

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1456-1.jpg"
    date="2022-06-10 14:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Trèfle à 5 feuilles"
    title="Certains ont même des feuilles enroulées."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1456-2.jpg"
    date="2022-06-10 14:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Trèfle à 4 feuilles"
    title="Cherchez bien, il y en a plus d'un."
  >}}
{{< /columns >}}

## Patates et haricots/pois

Les patates et les haricots/pois donnent de très belles fleurs et nous préparent aux futures raclettes que l'on pourra consommer une fois les patates récoltées.

Les plants résistent malgré les grosses pluies de ce mois.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1457.jpg"
    date="2022-06-10 14:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Fleurs de patates"
    title="Les patates font de jolies fleurs."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1458-1.jpg"
    date="2022-06-10 14:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Gousses de pois"
    title="Les gousses des pois/haricots font leur apparitions."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1458-2.jpg"
    date="2022-06-10 14:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Pois/haricots"
    title="Couleur 100% naturelle."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-13-1938.jpg"
    date="2022-06-13 19:38"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin 13 juin"
    title="Le 13 juin, la nature a bien entamé la saison chaude."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-23-1900.jpg"
    date="2022-06-23 19:00"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Plantes de patates"
    title="Les patates ont subis les intempéries mais survivront."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-25-1504.jpg"
    date="2022-06-25 15:04"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin 25 juin"
    title="Le 25 juin, la nature a bien entamé la saison chaude."
  >}}
{{< /columns >}}

## La vie de l'étang

De nouvelles variétés de plantes viennent à apparaître dans l'étang avec des tiges et des formes bien particulières.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1458-3.jpg"
    date="2022-06-10 14:58"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Etang juin"
    title="Les plantes se développent."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-10-1500.jpg"
    date="2022-06-10 15:00"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Etang juin grenouille"
    title="Les grenouilles y sont de plus en plus comblées."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-13-1937.jpg"
    date="2022-06-13 19:37"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Etang juin"
  title="De nouvelles fleurs émergent."
  >}}
{{< /columns >}}

## Balade en vélo avec Nalia

Yannis a fait l'acquisition d'un petit chariot à attacher au vélo. Celui-ci lui permet d'y mettre Nalia pour partir en balade. Etonnamment, Nalia n'a aucun souci avec ce moyen de transport.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-27-1915-1.jpg"
    date="2022-06-27 19:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia dans un chariot à vélo"
    title="Le premier voyage à vélo de Nalia."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-27-1915-2.jpg"
    date="2022-06-27 19:15"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia dans un chariot à vélo"
    title="Bien qu'un peu stressée, elle reste tranquille."
  >}}
{{< /columns >}}

# Juillet

Le beau, le (trop) chaud, on peut être en slip à longueur de journée, l'été est bien arrivé.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-31-1904.jpg"
    date="2022-07-31 19:04"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin 31 juillet"
    title="A la veille de la fête nationale, le maïs est plus grand que Matthieu."
  >}}
{{< /columns >}}

## Yannis commence son nouveau travail

Yannis commence son nouveau travail après quelques mois de repos. Mais heureusement, il n'oublie pas comment faire la fête.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-17-1644.jpg"
    date="2022-07-17 16:44"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis décuve canapé"
    title="Certains lendemains sont plus durs que d'autres."
  >}}
{{< /columns >}}

## Les plantes comestibles de l'étang et du jardin

La menthe aquatique nous aura permis de profiter de mojitos bien frais pendant ces grandes chaleurs !

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-13-1944.jpg"
    date="2022-07-13 19:44"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Menthe aquatique"
    title="En plus d'un haut lieu de biodiversité, l'étang fournit de la menthe à Mojito."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-31-1905.jpg"
    date="2022-07-31 19:05"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates 31 juillet"
    title="Les tomates produisent de lourds fruits."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-31-1906.jpg"
    date="2022-07-31 19:06"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates 31 juillet"
    title="Tentative de support à tomates."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-31-1907-1.jpg"
    date="2022-07-31 19:07"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates 31 juillet"
    title="Elles semblent délicieuses."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-31-1907-2.jpg"
    date="2022-07-31 19:07"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates 31 juillet"
    title="Ca pouuuuusse."
  >}}
{{< /columns >}}

## Accueil de voyageur-euse-s

Depuis plusieurs années, nous sommes inscrits sur plusieurs sites pour accueillir des voyageur-euse-s (Airbnb et Couch Surfing précédemment et maintenant Trustroots). Cette année encore, nous avons accueilli deux couples d'allemands très sympas. Il s'agit pour nous d'un moyen très convivial pour rencontrer de nouvelles personnes de façon différente. On passe toujours de très bonnes soirées et c'est souvent avec émotion que ces nouvelles connaissances nous quittent après avoir séjourné chez nous !

## Acquisition de fûts pour la brasserie

Des bouteilles aux canettes, c'est maintenant au tour des fûts de faire l'apparition à la brasserie ! Les gros cartons s'enchaînent à la coloc.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-29-1056.jpg"
    date="2022-07-29 10:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="2 Fûts"
    title="Les fûts seront utiles pour de futurs événements."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-07-29-1150.jpg"
    date="2022-07-29 11:50"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Carton accessoires brasserie"
    title="Et la tireuse à deux becs qui ira bien avec."
  >}}
{{< /columns >}}

## Début du sport pour Ludo

Ludo se met au sport... C'est tellement gros que ses connaissances ne le croient pas. Encore plus quand il s'agit de Sportfit (une variante du Crossfit). Et pourtant, au moins une fois par semaine, il s'y rend pour se dépenser. Jamais on aurait pu imaginer faire du sport de façon quotidienne, mais c'est vrai.

# Août

La belle saison a aussi son lot de surprises, notamment avec la sécheresse qui demande à rajouter de l'eau dans l'étang de façon régulière.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-01-0933-1.jpg"
    date="2022-08-01 09:33"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia jardin 1er août"
    title="Nalia, toujours avec son petit regard triste."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-01-0933-2.jpg"
    date="2022-08-01 09:33"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Etang 1er août"
    title="Les plantes de l'étang se dispersent."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-02-1818.jpg"
    date="2022-08-02 18:18"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Grenouille étang août"
    title="Les grenouilles ont de plus en plus de cachettes"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-27-1048-1.jpg"
    date="2022-08-27 10:48"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nénuphars étang"
    title="Les nénuphars couvrent quasiment toute la surface."
  >}}
{{< /columns >}}

## Acquisition d'une pompe pour le bidon de refroidissement

Une pompe permet de remettre l'eau dans le bidon de refroidissement. Le système en circuit fermé permet de refroidir la bière en utilisant la même eau.

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-08-01-1550.mp4"
    date="2022-08-01 15:50"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Pompe faisant circuler l'eau du bidon dans le refroidisseur à contre-courant"
    title="Grâce à une pompe, l'eau du bidon circule en circuit fermé pour refroidir la bière à la fin du brassage."
  >}}
{{< /columns >}}

## Les canettes explosives

Dû à une surpressurisation des canettes, tout un brassin (80+ canettes) a été perdu. Les canettes ont explosé à la cave, inondant le plafond et les murs de bière. Les canettes restantes ont été entreposées dans le jardin où, de temps à autre, elles explosaient en grand bruit.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-08-1950-1.jpg"
    date="2022-08-08 19:50"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une canette en aluminium de 33cl éclatée par le haut dû à la pression posée sur une table en bois"
    title="Déchiré."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-08-1950-2.jpg"
    date="2022-08-08 19:50"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Canette en surpression"
    title="A quelques minutes du drame."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-08-08-1950.mp4"
    date="2022-08-08 19:50"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Cannette en supression"
    title="Matthieu n'ose pas l'ouvrir (à raison)."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-08-11-2002.mp4"
    date="2022-08-11 20:02"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="canettes qui explosent"
    title="Ludo a risqué sa vie pour mettre fin aux explosions."
  >}}
{{< /columns >}}

## Le fumoir

Matthieu a fait l'acquisition d'un fumoir pour "_tout fumer_".

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-27-1047.jpg"
    date="2022-08-27 10:47"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Jardin en août"
    title="Ludo entretient le jardin et Matthieu vous fume."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-23-1903.jpg"
    date="2022-08-23 19:03"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Cocktail en terrasse"
    title="Matthieu concocte de délicieux cocktails."
  >}}
{{< /columns >}}

## Récoltes

Malgré les vilaines limaces, nous avons pu savourer quelques récoltes.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-27-1049.jpg"
    date="2022-08-27 10:49"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates"
    title="Les tomates rougissent mais d'autres variétés restent jaunes."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-27-1059.jpg"
    date="2022-08-27 10:59"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Récolte de patates"
    title="Quelques 7 kg de patates serviront aux raclettes/frites maison."
  >}}
{{< /columns >}}

## Déjeuners sur la terrasse

Une météo plus clémente qu'en 2021 (cf [les inondations de 2021](/actualites/qu-est-ce-que-le-temps-passe-vite/#la-pluie-et-les-inondations)) permet d'agréables déjeuners en terrasse.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-08-28-0928.jpg"
    date="2022-08-28 09:28"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Petit déjeuner sur la terrasse"
    title="La tresse maison de Matthieu régale nos papilles."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-08-06-0941.mp4"
    date="2022-08-06 09:41"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia donne la patte"
    title="Nalia est toujours autant intéressée par la nourriture."
  >}}
{{< /columns >}}

## Début du sport pour Matthieu

Matthieu commence lui aussi le sport mais sur Fribourg, proche de son lieu de travail. Trois fois par semaine, il soulève de la fonte pour devenir la table qu'il a toujours imaginée.

# Septembre

Le mois de septembre laisse place à de jolies promenades et un temps assez doux. Nalia aussi profite de la nature autour de notre maison.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-09-15-1817.jpg"
    date="2022-09-15 18:17"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia et un poisson mort dans l'eau"
    title="Si on la laissait, Nalia serait ravie de pouvoir manger ce beau gros poisson mort."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-09-04-2248.mp4"
    date="2022-09-04 22:48"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia et des chèvres"
    title="Nalia veut jouer avec \"ces chiens à cornes\"."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-09-14-1921.mp4"
    date="2022-09-14 19:21"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un éclair tombe au loin"
    title="Les soirées du mois de septembre offre encore de sacrés orages et de gros éclats magnifiques sur le lac."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-09-29-1233.mp4"
    date="2022-09-29 12:33"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une petite musaraigne dans un linge"
    title="Yannis a sauvé une petite musaraigne qui était tombée dans un trou et qui n'arrivait pas à en ressortir."
  >}}
{{< /columns >}}

## Les plants de tomate ont mieux vécu que l'année passée

Contrairement à l'année passée où nos douze plants de tomate étaient morts à cause des inondations, nos trois plants de cette année ont plutôt bien tenu le coup !

Nous avons même pu réaliser quelques salades de tomates ! Nous avons une photo mais nous avons oublié de l'ajouter dans cette actualité. Si vous nous le demandez, on peut vous l'envoyer en privé.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-09-21-1304-1.jpg"
    date="2022-09-21 13:04"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates prêtes à être récoltées"
    title="Prêtes à être récoltées, jaunes ou rouges."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-09-21-1304-2.jpg"
    date="2022-09-21 13:04"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Tomates prêtes à être récoltées"
    title="Des variantes de formes sur le même plant, une hybride ?."
  >}}
{{< /columns >}}

## Inauguration des premiers fûts de la brasserie

Le 24 septembre, nous avons organisé une fête dans le jardin avec la tonnelle, les deux fûts contenant les fonds de tiroir de la brasserie avec notamment la bière "Yannis m'a troll" et la "Je trolle Yannis" et la nouvelle tireuse acquérie pour l'occasion et le BBQ à l'extérieur.

Petite fête sympa où le temps nous permet encore de profiter de la terrasse !

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-09-24-1853.jpg"
    date="2022-09-24 18:53"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les nageurs dans le lac de Neuchâtel"
    title="Les courageu-ses-x nageu-euse-r-s vont se rafrachir au lac."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-09-25-1002.jpg"
    date="2022-09-25 10:02"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia dans le jardin"
    title="Nalia garde sagement le costume de bain oublié de Miguel."
  >}}
{{< /columns >}}

# Octobre

Les journées d'octobre permettent encore de profiter du soleil avant le début de l'hiver et du brouillard.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-18-1756.jpg"
    date="2022-10-18 17:56"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les deux fermanteurs de la brasserie"
    title="Nalia connaît tellement bien la Grande Cariçaie que c'est elle qui nous promène lors de ses balades."
  >}}
{{< /columns >}}

## Début du sport pour Yannis

Et c'est finalement au tour de Yannis de se mettre au sport au même endroit que Ludo, de façon plus ou moins régulière !

## Les dernières récoltes

Le mois d'octobre marque un peu la fin des récoltes de l'été, notamment avec ce radis et une citrouille qui finira en soupe.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-28-1412.jpg"
    date="2022-10-28 14:12"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un radis en terre"
    title="Ce fort radis sera mangé par Yannis."
  >}}
{{< /columns >}}

## Brassage de la bière de Noël

Matthieu et Yannis démarrent le brassage de la future (bonne) bière de Noël.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-09-1621-1.jpg"
    date="2022-10-09 16:21"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les deux fermanteurs de la brasserie"
    title="L'acquisition du second fermenteur permet de brasser plus de bière à la fois."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-09-1621-2.jpg"
    date="2022-10-09 16:21"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un sample de la bière avant fermentation"
    title="Ce brevage permet d'avoir une idée du goût final de la bière avant sa fermentation."
  >}}
{{< /columns >}}

## Récupération du thé de vers

Depuis plusieurs années (avant même d'habiter dans la région), nous avons un petit lombricompost fabriqué à la main qui nous permet de composter nos déchets végétaux.

Celui-ci produit ce qu'on appelle du _"thé de vers"_ qui est le jus/l'humidité produit par les vers de terre. Ce thé peut être récupéré puis dilué avec de l'eau pour faire office d'engrais.

Nous avons pu remplir plusieurs bocaux de ce thé donc si vous en souhaitez, on peut vous en donner ! Il suffit de le diluer à raison de 10% thé / 90% eau puis d'arroser vos plantes pour les booster.

Nous avons pu valider avec deux amis qu'un transplant de vers est possible afin de récréer une nouvelle colonie. Il est important de ne donner que des légumes pour éviter de les tuer par contre. Si vous souhaitez démarrer votre propre lombricompost, on peut aussi vous donner quelques vers.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-13-1307.jpg"
    date="2022-10-13 13:07"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Ludo devant des bocaux de thé de vers"
    title="Ludo est fier du travail accompli."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-13-1308-1.jpg"
    date="2022-10-13 13:08"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Dessins au fond d'une caisse"
    title="La bonne soupe de ce soir."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-13-1308-2.jpg"
    date="2022-10-13 13:08"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Dessins au fond d'un caisse"
    title="Un tableau qui vaudra des millions."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-13-1308-3.jpg"
    date="2022-10-13 13:08"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Lombricompost"
    title="Plusieurs bocaux à donner grâce aux vers."
  >}}
{{< /columns >}}

## La résolution du mystère des esprits du jardin ?

Les chaussures déplacées, des câbles électriques coupés, des bruits durant la nuit... Que de mystères autour de notre maison ! Nous sommes arrivés à la conclusion que ça devait être des fouines, renards ou autres animaux qui déplaçaient nos chaussures et non les esprits du jardin (quoi que...).

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-10-19-1335.mp4"
    date="2022-10-19 13:35"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis explique le sort de la fontaine solaire"
    title="La fontaine solaire a sans doute sucombé au sort des fouines..."
  >}}
{{< /columns >}}

## Deuxième séance pour l'identité visuelle de la brasserie

Les couleurs, le style et le design général de l'identité de la brasserie ont été discutés durant cette réunion entre Mathilde, Jess et Matthieu. Des résultats prometteurs sont sortis de ces discussions.
 
{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-23-1330.jpg"
    date="2022-10-23 13:30"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Séance brasserie MFP"
    title="Les idées fusent."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-10-23-1333.jpg"
    date="2022-10-23 13:33"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Séance brasserie MFP"
    title="Une présentation digne d'une keynote Apple."
  >}}
{{< /columns >}}

# Novembre

Le rythme de la coloc a toujours été très chill. Une vraie petite vie de famille avec ses meilleurs potes et un gentil chien, que demander de plus.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-11-08-1110.jpg"
    date="2022-11-08 11:10"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Nalia sur les genoux de Ludo en train de travailler"
    title="Nalia accompagne Ludo dans son travail lorsque Yannis est au bureau."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-11-08-1603.mp4"
    date="2022-11-08 16:03"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis qui rentre du travail avec sa trotinette"
    title="Yannis qui rentre du travail avec sa trotinette de l'estrême."
  >}}
{{< /columns >}}

## Le premier prototype du logo de la brasserie

Suite aux réunions de l'équipe de choc, le premier prototype de logo est réalisé par Mathilde ! Vous en voyez le résultat ci-dessous.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-11-21-2028.jpg"
    date="2022-11-21 20:28"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le premier aperçu du logo de la brasserie"
    title="Mathilde a réalisé le premier prototype du logo de la brasserie suite au workshop avec Jess et Audrey."
  >}}
{{< /columns >}}

## Prototypes des supports à panneaux solaires

Les prototypes des supports à panneaux solaires ont été conçus par Ludo. Partant d'une bonne idée pour assurer le maintien des panneaux sur le toit du cabanon à outils (qui a été réaménagé d'ailleurs), il s'est rendu compte de pourquoi personne n'utilisait sa technique pour faire des angles droits.

Les supports sont fonctionnels mais Ludo a le souhait de les refaire pour assurer que les panneaux ne s'effondreront pas avec les intempéries et le vent.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-11-04-1906.jpg"
    date="2022-11-04 19:06"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Les prototypes de support pour les panneaux solaires"
    title="Les prototypes de support devront sans doute être refaits pour s'assurer de leur solidité."
  >}}
{{< /columns >}}

## Le début des feux

Le mois de novembre amène avec lui les premiers feux. Avec les restrictions énergétiques, nous avons pris la décision de faire un feu que s'il faisait plus de 14°C dans la maison, un degré de moins par rapport à l'hiver passé. Le froid n'est pas du tout un problème pour nos amis et souffrent parfois de la chaleur lorsqu'ils ne sont pas chez eux.

Nous avons décidé de ne pas racheter de bois pour le moment et finir la réserve acquise l'année passée. Nous verrons si c'est suffisant mais lors de la rédaction de cet article, il ne manque pas de bois et Ludo est en short dans la maison, ce qui est plutôt bon signe.

## La récidive des canettes explosives

Le mauvais batch de cet été a permis à Matthieu et Yannis d'améliorer leur processus de fermentation en canette. Malheureusement, il reste encore un petit pourcentage de canettes qui ont explosé après la mauvaise expérience de cette année.

Une canette a même explosé durant la nuit juste à côté de la chambre de Matthieu ! On vient peut-être d'inventer une nouvelle forme de réveil..?

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-11-21-1818.mp4"
    date="2022-11-21 18:18"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le reste d'une canette au fond de l'évier"
    title="James a malheureusement expérimenté l'explosion d'une canette dans son sac à dos..."
  >}}
{{< /columns >}}

# Décembre

Le dernier mois de l'année est déjà là et la pression monte pour rédiger un article pour le petit hanneton. Heureusement, nous savons que la pression ne se subit pas, elle se boit.

## Apéro de Noël

L'apéro de Noël organisé par Matthieu aura permis de goûter la bière préparée pour l'événement. Plusieurs ami-e-s et voisin-e-s sont venus pour l'occasion et nous avons pu passer une très chouette soirée ! Yannis a un peu regretté le lendemain mais ça valait la peine.

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-12-16-1245.jpg"
    date="2022-12-16 12:45"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Une canette de bière de la brasserie MFP avec son sticker spécial Noël"
    title="Les stickers de la brasserie sont arrivés juste à temps pour l'apéro de Noël !"
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-12-17-1602.jpg"
    date="2022-12-17 16:02"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le mocktail pour l'apéro"
    title="Le _mocktail_ (nom donné à un cocktail sans alcool) rafraichissant pour les convives."
  >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-12-17-1613.mp4"
    date="2022-12-17 16:13"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Le vin chaud en préparation"
    title="Matthieu s'est (évidemment) inspiré des meilleures recettes pour son vin chaud."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-12-18-1430.jpg"
    date="2022-12-18 14:30"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Yannis et Nalia qui dorment sur le canapé"
    title="Yannis dort sur le canapé avec Nalia après une soirée un peu trop arrosée."
  >}}
{{< /columns >}}

## Problème de pompe à la cave

La nuit du 24 décembre a amené avec elle un problème avec la pompe de la cave (super le cadeau de Noël). Celle-ci ne s'arrêtait plus et continuait à pomper dans le vide.

Après un travail assez conséquent pour démonter la tuyauterie associée, nettoyer la pompe et enlever le sable stagnant au fond du trou, nous avons pu la faire repartir et évacuer le sable restant. Fâcheusement, celui-ci a bouché à la sortie l'autre canalisation d'évacuation... Un problème à résoudre lorsque le temps nous le permettra !

{{< columns >}}
  {{< video
    src="/videos/actualites/recapitulatif-2022/2022-12-24-0849.mp4"
    date="2022-12-24 08:49"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="La pompe n'arrive plus à s'arrêter"
    title="La pompe n'arrive plus à s'arrêter et une mousse inconnue est présente dans le fond."
  >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-12-24-0852.jpg"
    date="2022-12-24 08:52"
    column-classes="is-half"
    figure-classes="has-ratio m-0"
    alt="Un triton à la cave"
    title="Ce n'est pas la première fois que l'on retrouve des tritons à la cave. Nous ne savons pas d'où ils viennent mais nous pourrons les mettre à l'étang dorénavant !"
  >}}
{{< /columns >}}

# Les plans pour l'année prochaine

Pour l'année prochaine, Ludo a le souhait de beaucoup plus s'investir dans le jardin. Nous avons plusieurs idées pour aménager des cadres de palettes comme jardin surélevés, enfin installer les panneaux solaires, remettre la pompe solaire et finalement avoir une vraie récolte.

Comme toujours, il s'agit d'un immense plaisir pour nous de vous rédiger ces articles du petit hanneton. Nous espérons pouvoir vous donner plus de nouvelles plus régulièrement mais nous verrons comment se passe l'année prochaine !

N'hésitez pas à nous écrire ou à passer à la maison si le coeur vous en dit, on a toujours de bonnes bières et Nalia pour vous accueillir. :wink:

En vous souhaitant une excellente année 2023,  
Ludo, Matthieu, Yannis et Nalia

{{< columns >}}
  {{< img
    src="/images/actualites/recapitulatif-2022/2022-06-13-1936.jpg"
    column-classes="is-full"
    figure-classes="has-ratio m-0"
    alt="Nalia dans le jardin avec un bâton"
    title="Nalia se réjouit de vous retrouver l'année prochaine !"
  >}}
{{< /columns >}}

PS: Un grand merci à Yannis qui m'a aidé à finaliser cette actualité après deux jours de travail pour vous le délivrer avant 2023 !
