---
title: "Le jour J !"
description: "Aujourd'hui est un grand jour !"
author: "Le petit hanneton"
kind: "page"
draft: false
date: 2020-11-22T14:00:00+02:00
banner: "/images/le-jour-j-banner.jpg"
---

Ça y est, nous y sommes. Le jour du lancement "officiel" est arrivé !

Après quatre mois de travail, on se sent enfin prêts de vous présenter
officiellement le projet du petit hanneton.

On ne va pas se mentir, nous sommes fiers que ce projet devienne une réalité.

Nous avons encore pleins d'idées et de projets mais surtout, on espère que cela
vous inspira aussi, que vous aurez envie de venir nous rencontrer, que l'on
puisse discuter et partager autour d'une bière.

Fouillez un peu, faites comme à la maison et n'hésitez pas à nous contacter si
vous avez une remarque ou un commentaire ! On attend vos retours avec impatience
!

Ludo, Matthieu et Yannis
