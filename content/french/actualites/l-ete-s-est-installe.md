---
title: "L'été s'est installé..."
description: "Et le petit hanneton pointe le bout de son nez"
author: "Le petit hanneton"
kind: "page"
draft: false
date: 2021-06-21T10:00:00+02:00
banner: "/images/actualites/l-ete-s-est-installe/banner.jpg"
---

On l'attendait, on l'avait presque oublié. Mais il est là, il est revenu et il tape plus fort que jamais: le soleil ! Le mois de mai pluvieux et frais laisse sa place au beau et au chaud.

De notre côté, BEAUCOUP de choses ont pris forme dans le jardin. Ça devient difficile de se souvenir de tout !

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/cygnes.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Des cygnes et leur progéniture"
    title="La nature s'est réveillée et les petits cygnes profitent du lac sous la surveillance de leurs parents."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/moustiquaires.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="La moustiquaire dans le fameux escalier"
    title="Les moustiques sont de retour et des moustiquaires ont été posées pour les garder dehors."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/magnolia.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="La moustiquaire dans le fameux escalier"
    title="De nombreuses fleurs ont vu le jour pendant le printemps, dont le magnolia."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/abricotier.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="La moustiquaire dans le fameux escalier"
    title="Un abricotier, qui était passé jusqu'ici inaperçu, a donné ses fleurs."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/jardin-avant.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Jardin vu depuis le premier étage"
    title="Le jardin a connu une certaine transformation depuis notre arrivée (avant)."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/jardin-apres.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Jardin vu depuis le premier étage"
    title="Le jardin a connu une certaine transformation depuis notre arrivée (après)."
  >}}
{{< /columns >}}

# Quoi d'neuf docteur ?

## L'organisation du cabanon à outils est revue

En mars, l'intérieur du [cabanon à outils]({{< ref "/projets-realises/cabanon-a-outils" >}} "cabanon à outils") a été complétement revu. Les tablards ont été inversés et un plan de travail permet de poser ses affaires. L'entiéreté du cabanon a été réfléchi pour qu'il y soit pratique de travailler et entreposer les différents outils pour le jardin.

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/cabanon-a-outils.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Intérieur du cabanon"
    title="Le cabanon abrite les quelques outils que l'on possède pour entretenir le jardin ainsi que la paille pour les buttes."
  >}}
{{< /columns >}}

## Système de récupération d'eau de pluie

Bien que déjà commencé l'année passée, le système de récupération d'eau à base de bidons est maintenant terminé et fonctionnel. Quatre bidons en série permettent de récolter plus d'un mètre cube (1m³) d'eau de pluie que l'on peut stocker et utiliser lors des périodes de sécheresse ou simplement lorsqu'il n'a pas plu pendant plusieurs jours. Presque pleins après le mois de mai pluvieux, ils sont maintenant au 2/3 de leur capacité totale. Les détails de construction ainsi que différentes ressources sont disponibles sur [la page du projet]({{< ref "/projets-realises/systeme-de-recuperation-d-eau" >}} "système de récupération d'eau").

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/bidons-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Mathilde et Ludo mesurent les bidons"
    title="Les mesures sont effectuées sur les bidons pour y placer la sortie d'eau."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/bidons-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Mathilde aide Ludo dans le baril à fixer la plomberie"
    title="La plomberie est installée sur les bidons afin de les relier entre eux."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/bidons-3.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Les bidons reliés se remplissent d'eau"
    title="L'eau récoltée est répartie entre les bidons, chacun d'une capacité de 280 litres."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/bidons-4.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Ludo en train de fixer l'embout Gardena"
    title="Un embout Gardena est fixé sur le premier baril pour y mettre un tuyau d'arrosage."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/bidons-5.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Matthieu et Ludo en train de tester l'arrosage avec le tuyau"
    title="Le premier test avec le tuyau d'arrosage est peu concluant, mais on s'amuse quand-même."
  >}}
{{< /columns >}}

## La spirale aromatique

Grâce aux MULTIPLES tuiles laissées par les anciens propriétaires, nous avons construit une "spirale aromatique". De part sa construction, certaines plantes vont bénéficier de plus de soleil que d'autres et à l'inverse, protéger certains plantes de recevoir trop de soleil. Cela crée un petit écosystème où les plantes plus fragiles seront protegées du soleil par les autres plantes et / ou par la structure de la spirale et permet d'avoir en un seul endroit toutes les plantes pour préparer de bons plats.

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/spirale-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Spirale en construction"
    title="Le pilier central permet de garder l'humidité et le frais de la spirale."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/spirale-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Spirale en construction"
    title="Les tuiles sont utilisées pour créer la structure extérieure."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/spirale-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Spirale en construction"
    title="Des cailloux et de la terre remplissent l'antre de la spirale."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/spirale-4.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Spirale terminée"
    title="La petite spirale terminée, ses créateurs prennent le temps de l'admirer."
  >}}
{{< /columns >}}

Profitant des différentes cavités de la structure, nous avons commencé à constuire un hôtel à insectes pour y accueillir les différents pollinisateurs de notre jardin. Bien qu'on ait semé de nombreuses variétés d'aromates, peu ont germés. La spirale comprend malgré tout quelques plantes qui poussent lentement mais sûrement. 

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/spirale-hotel.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Roseaux pour les insectes"
    title="Des roseaux, du bois creux et des pives permettent aux insectes d'y faire un abri."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/spirale-plante.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Plante qui sort de terre"
    title="Les premières plantes commencent à sortir de la spirale."
  >}}
{{< /columns >}}

Pour tous les détails, voir [la page dédiée à la spirale aromatique]({{< ref "/projets-realises/spirale-aromatique" >}} "spirale aromatique").

## La troisième butte de permaculture

Pendant que Ludo était en vacances au Tessin, Matthieu et Yannis ont réalisé la troisième [butte de permaculture]({{< ref "/projets-realises/potager-en-permaculture" >}} "butte de permaculture") à l'aide de terre et de BRF (bois raméal fragmenté) achetés à la compostière de Cheyres. Celle-ci, plus large, complète les deux premières buttes réalisées au mois de novembre. Bien qu'à la base une quatrième butte était planifiée, cette dernière aurait été trop souvent à l'ombre.

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/troisieme-butte-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Tas de BRF"
    title="Le BRF permet de mieux garder l'eau dans la terre grâce à un effet \"éponge\"."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/troisieme-butte-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les trois buttes"
    title="La dernière butte est beaucoup plus large que les précédentes."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/troisieme-butte-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="L'installation des poteaux tuteurs"
    title="Des poteaux ont été placés pour tirer un fil."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/troisieme-butte-4.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Matthieu qui accroche les ficelles"
    title="Matthieu accroche les ficelles au fil principal qui vont permettre de soutenir les plants de tomates."
  >}}
{{< /columns >}}

## Plantons les plantons

La venue du soleil a provoqué une explosion de la pousse des plantes du jardin. Les semis qui ont germés ont été transplantés dans les buttes après les saints de glace.

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/mise-en-terre-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Matthieu en train d'accrocher les ficelles tuteurs"
    title="Matthieu accroche les ficelles aux plants de tomates."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/mise-en-terre-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les plants de tomates et Nalia"
    title="Tous les plants sont attachés et Nalia surveille la pousse."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/mise-en-terre-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="La butte avec les poteaux"
    title="Les ficelles sont une bonne alternative aux tuteurs qui peuvent être adaptés selon le stade de la plante."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/mise-en-terre-4.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les piments et poivrons sur la butte"
    title="Les piments et les poivrons avec les patates qui ne sont pas encore sorties de terre."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/mise-en-terre-5.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Matthieu en train de transplanter les plantons"
    title="La transplantation est un travail délicat qui demande du temps et de la patience."
  >}}
{{< /columns >}}

Malheureusement, il a encore fait frais et humide pendant deux semaines et les plantons ont mis beaucoup de temps à s'adapter à leur nouvel environnement. Pendant quelques temps, ils semblaient ne plus vouloir grandir et avaient passablement mauvaise mine. Certains plantons ont été emportés par les limaces, qui n'oeuvrent que de nuit, à l'abri des regards, et ne laissent derrière elles qu'une parcelle de butte vierge de verdure...

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/limaces-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Une limace en train de manger un pauvre planton"
    title="Les limaces semblent autant apprécier les plantes que nous..."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/limaces-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Un planton, victime des limaces"
    title="... mais n'ont pas la patience d'attendre qu'elles soient prêtes pour les dévorer."
  >}}
{{< /columns >}}

Heureusement, d'autres plantons, plus vaillants, ont tenu le coup et semblent décidés à prospérer dans ce nouvel espace qui leur est dédié. Les premières tomates commencent à apparaître, les patates se portent à merveille et les oignons grandissent de jour en jour et rien ne semblent les arrêter.

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/tomates.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Tomates"
    title="Les tomates se sont remises de l'expérience traumatisante de la transplantation."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/cornichon.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Cornichon"
    title="Le cornichon ressemble beaucoup aux plants de courgette et se porte très bien."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/patates.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Patates"
    title="Les patates qui avaient germé se plaisent parfaitement dans leur coin de butte."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/oignons.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Oignons"
    title="Les oignons germés dans nos placards atteignent des hauteurs impressionnantes."
  >}}
{{< /columns >}}

De plus, d'autres plantons qui avaient été gardés "au cas où" comme solution de secours continuent à bien aller et cherchent actuellement de nouveaux propriétaires qui seraient prêts à les adopter et les planter chez eux !

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/plantons-dans-la-veranda-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les plantons dans la veranda"
    title="Il nous reste de nombreux plantons de tomates qui cherchent avidemment une nouvelle maison."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/plantons-dans-la-veranda-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les plantons dans la veranda"
    title="_\"S'il-vous-plaît, adapotez-nous, on est gentils et on demande pas beaucoup d'entretien.\"_"
  >}}
{{< /columns >}}

Grâce à l'eau de pluie récoltée pendant le mois de mai, nous pouvons arroser le jardin sans difficulté à l'aide de nos arrosoires sans utiliser l'eau courante de la maison.

Bien que la plupart des semis qui avaient directement été mis en terre n'ont simplement pas pris, les buttes semblent bien partie pour le reste de l'année ! D'autres plantes pourront venir densifier les cultures au fur et à mesure de la saison et, pour une première expérience de jardinage, nous ne sommes pas peu fiers du résultat !

Plantes espérées et résultats:

- Après un début difficile (nous avons oublié de tremper les racines pendant 24h, conseil qui semble aider à une bonne adaptation), les plants de tomates ont bien pris et grandissent
- Les poivrons et piments ont plus de peine et certains n'ont plus que la tige, merci les limaces
- Les salades et radis n'ont pas germés ou se sont fait manger
- Les patates et les oignons sont parmis ceux qui se portent les mieux
- Les raisinets se développent bien, les pommes et abricots aussi
- Les framboises survivent tant bien que mal et certaines sont bientôt prêtes pour être mangées

## Le coin des plantes grimpantes

La glycine et le palmier avaient laissé un vide dans le coeur de nombreuses personnes... Mais c'était sans compter le muguet qui a surgit des profondeurs de la terre. Son système de racines, toile épaisse de 20 cm de profondeur, doit être enlevé pour y mettre d'autres plantes. Après avoir retourné la terre, l'entiéreté du muguet a été enlevé pour y accueillir les futurs haricots, du maïs et des courges. Le maïs a pris du temps pour sortir et les haricots poussent à une vitesse impressionnante.

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/plantes-grimpantes-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Coin de terre avec de la paille"
    title="La terre préparée, elle est protégée du soleil avec de la paille."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/plantes-grimpantes-2.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Coin de terre sans paille"
    title="La paille protége bien du soleil, mais malheureusement pas du vent."
  >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/plantes-grimpantes-3.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Les haricots et le maïs"
    title="Après avoir mis du compost et semer les graines, les plants poussent !"
  >}}
{{< /columns >}}

## La brasserie met les bouchées doubles

La brasserie s'est remise en route à vitesse de croisière: 30 litres sont brassés toutes les deux semaines avec une triple (9%) qui sera bientôt prête à la dégustation et la Ludovice ainsi que la Yannis Pale Ale qui le seront dans quelques semaines.

Fini les bouteilles ! Nous avons décidé de nous lancé dans la laborieuse expérience des canettes. Après s'être rendu compte qu'il existe deux grandes familles de canette et que nous avions pas la bonne pièce. Nous avons pu nous lancer dans le réglage de la machine. Le résultat à l'air satisfaisant nous attendons encore de voir comment ça évolue car il semblerait qu'il ait quelques pertes.

Le passage aux cannettes a permis d'accélérer le processus grâce à une encaneuse et le stockage en est facilité; les bières pouvant se mettre les unes sur les autres, elles prennent moins de place.

La [page de la brasserie]({{< ref "/projets-realises/brasserie" >}} "brasserie") a été mise à jour et comprend de nombreuses informations sur les nouvelles bières, h'hésitez pas à aller jeter un coup d'oeil !

_Le grand retour de la Ludovice (se prononce à l'italienne, on est pas dans GTA Vice City Stories), une porter aux arômes de cacao amer, est en canette. Cette bière noire peu pétillante se dégustera parfaitement avec vos desserts. Température de service conseillé aux alentour de 12°C._

_Plongez dans la légende de Freijdis Eirikrsdottir, la fille d'Eirikr Thorvaldson. Femme de caractère, on raconte que lors d'une expédition au Canada, elle aurait tué une dizaine d'islandais car ils l'aurait agacée. C'est pourquoi cette blonde est forte, puissante et risque de vous faire trébucher. Son délicat parfum saura-t-il vous envoûter ?_

Le brasseur Matthieu et son assitant Yannis font tout pour vous offrir les meilleures bières, made in Cheyres. :wink:

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/brasserie-1.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Canettes empilées à la cave"
    title="Les canettes sont beaucoup plus simple à stocker que les bouteilles."
  >}}
{{< /columns >}}

## On laisse les ruches aux pros

Le projet de ruche a été abandonné, en tout cas pour le moment. Après avoir discuté avec des passionnés de la branche, les abeilles domestiques demandent plus d'entretien qu'imaginé. Un mauvais entretien met plus en périle les autres ruches et apiculteurs car le risque de propagation des maladies est plus élevé. Nous resterons donc aux hôtels à insectes que l'ont va construire pour nos amis les pollinisateurs.

# Projets futurs

## Logo et panneaux informatifs du petit hanneton 

Le logo du petit hanneton devrait être terminé grâce à une pote aux qualités de design hors-pair. Différents panneaux informatifs que l'on souhaite mettre autour de la maison sont eux aussi quasi terminés et viendront accueillir de nouveaux visiteurs dans notre jardin.

## Les rideaux (ENFIN)

C'était prévu, c'était prédit, les rideaux épais qui auraient dû être terminés cet hiver n'auront pas pu être finis avant la fin de celui-ci. Mais grâce à un système ultra-sophistiqué utilisant des pincettes, ceux-ci nous ont quand-même tenus chauds et garder le froid à l'extérieur. Il est maintenant temps de les terminer et être prêt pour l'hiver prochain. Et sinon, ça sera pour l'année prochaine.

## Le coin à salades

Nous avons pu récupérer un bac à légumes chez un pote (merci David !) que nous souhaitons mettre en place pour y faire pousser des salades à hauteur de bras. Celui-ci est monté mais il est encore nécessaire d'y mettre la terre et le préparer avant de pouvoir semer les salades.

## La nouvelle parcelle de terre et son étang

L'hiver dernier, des scouts nous avaient débarrassé du gravier autour du cabanon à outils. De la terre a été remise sur une moitié de la surface et l'autre sera dédiée à un étang pour les grenouilles, herissons et autres bêtes (svp les moustiques, pas ici).

## Installation des panneaux solaires

Les panneaux solaires sont prêts à être installés mais plusieurs éléments ont dû / doivent être vérifiés avant de pouvoir le faire. On espère pouvoir les mettre prochainement et ainsi profiter encore plus de ce soleil !

## Systèmes de récupération d'eau - PART 2

Le système de récupération d'eau présenté dans cette actualité n'est que le premier sur les trois que l'on souhaite mettre en place. Toutes les goutières doivent être utilisées !

# Autres communications

Le [cabanon a troc]({{< ref "/projets-realises/cabanon-a-troc" >}} "cabanon a troc") à un succès limité pour l'instant, n'hésitez pas à passer !

# Mot de fin

Avec la saison qui avance, on espère pouvoir bientôt profiter des premières récoltes (et des super sorbets au raisinets de Yannis). Avec l'expérience acquise, des idées pour l'année prochaine seront à tester pour rendre le jardin encore plus beau. Des projets sous le coude, on se réjouit de vous partager les résultats d'ici peu de temps !

A tout bientôt,

Ludo, Matthieu, Yannis et Nalia

{{< columns >}}
  {{< img
    src="/images/actualites/l-ete-s-est-installe/nalia.jpg"
    column-classes="is-half"
    figure-classes="is-3by4 m-0"
    alt="Nalia"
    title="Nalia trouve toujours de nouveaux moyens de nous surprendre, comme ici, où elle cherche dans les endroits les plus inattendus un coin de fraicheur pour mieux supporter le chaud."
  >}}
{{< /columns >}}
