---
title: "Soirées bières grillades été 2024"
description: "Viens déguster des bières et passer un bon moment !"
author: "Brasserie MFP x Le petit hanneton"
kind: "page"
draft: false
date: 2024-04-25T20:00:00+02:00
banner: "/images/actualites/soirees-biere-grillades-2024/banner.jpg"
---

On prend les même et on recommence, viens nous rejoindre sur notre terrasse/jardin.

Les dates :
- Vendredi 24 mai dès 17h
- Samedi 29 juin dès 12h
- Vendredi 19 juillet
- Vendredi 9 août
- Vendredi 30 août

Les vendredis dès 17h et les samedis dès 12h !

Au programme :
- Dégustation de bières artisanales que l'on brasse avec amour, 2 bières à la pression
- Un grill à disposition et snacks canadiens à partager.

Il y aura également une crousille sur place pour soutenir la brasserie et continuer à créer des bières inédites.

Si t'es partant(e), ramène tes potes, ta bonne humeur et un plat à partager, et rejoins-nous pour une soirée cool et conviviale. Annonce nous ta venue, ça nous aidera à mieux préparer l'évènement et à garantir que tout le monde passe une super journée/soirée.

Il sera toujours possible de réparer des objets sur demande préalable alors n'hésitez pas !

Viens boire une bière, manger un burger, réparer ton vélo ou ton grille-pain et soutenir notre brasserie, tout ça en bonne compagnie. C'est l'occasion idéale pour boire un coup entre potes, se détendre et profiter de la fin de la semaine.

On espère te voir bientôt !

Bisous

https://brasserie-mfp.ch/fr/blog/soiree-craft-n-fix/

Restez au courant des dernières infos sur les soirées Craft'n'fix sur Telegram
<div class="container">
    <div class="content">
    <span class="icon ml-3 mr-3"><a href="https://t.me/+uqlHfL2PXD04ZjU0"><i
                class="fab fa-3x fa-telegram"></i></a></span>
    </div>
</div>
