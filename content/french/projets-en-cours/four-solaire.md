---
title: "Four solaire"
description: "Faire la cuisine grâce à la chaleur du soleil"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/four-solaire-banner.jpg"

tags:
  - "Cuisine"
  - "Four"
  - "Solaire"
---

_Cette page doit encore être rédigée. Si vous souhaitez donner un coup de main / avoir plus d'informations avant la rédaction de la page, n'hésitez pas à nous contacter à l'aide de la page de [Contact]({{< ref "/a-propos/contact" >}} "Contact") !_

A partir de bois récupéré sur les meubles âbimés de l'atelier de la cave, de vitre et autres matériaux trouvés aux alentours.
Un four solaire de taille et poids raisonnable basé sur des plans [plan libre d'accès](https://www.planfoursolaire.com) pouvant chauffer jusqu'à 180°C grâce au soleil.
