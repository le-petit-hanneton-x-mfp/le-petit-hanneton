---
title: "Projets en cours"
description: "Les projets en chantier"
kind: "page"
layout: "activites"

cascade:
  banner: "/images/projets-en-cours-banner.jpg"
---

Les projets listés ici peuvent changer à tout moment, si vous ne trouvez plus un
des projets qui vous intéresse vérifiez dans les
[Projets réalisés](/projets-realises/).
