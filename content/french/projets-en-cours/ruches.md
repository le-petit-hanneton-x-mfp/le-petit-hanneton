---
title: "Ruches"
description: "Les pollinisatrices directement à côté du jardin..."
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/ruches-banner.jpg"

tags:
  - "Abeille"
  - "Ruche"
  - "Pollinisateurs"
---

Le projet de ruche a été abandonné, en tout cas pour le moment. Après avoir discuté avec des passionnés de la branche, les abeilles domestiques demandent plus d'entretien qu'imaginé. Un mauvais entretien met plus en périle les autres ruches et apiculteurs car le risque de propagation des maladies est plus élevé.

En attendant l'[hotel]({{< ref "/projets-realises/spirale-aromatique" >}} "hôtel à insectes") devrait accueillir quelques abeilles sauvages et autres pollinisateurs.