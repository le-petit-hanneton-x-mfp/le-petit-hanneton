---
title: "Barils à composter"
description: "Transformer le compost en terre le plus rapidement possible !"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/barils-a-composter-banner.jpg"

tags:
  - "Compost"
---

_Cette page doit encore être rédigée. Si vous souhaitez donner un coup de main / avoir plus d'informations avant la rédaction de la page, n'hésitez pas à nous contacter à l'aide de la page de [Contact]({{< ref "/a-propos/contact" >}} "Contact") !_

Pour l'instant, un baril noir de 280 litres fermé sert de composteur pour les déchets organiques en surplus ou inadaptés au lombricompost.

Il n'est pas totalement équilibré, probablement un peu trop acide mais nous essayons de faire au mieux.
