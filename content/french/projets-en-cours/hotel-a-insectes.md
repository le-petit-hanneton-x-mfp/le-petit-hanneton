---
title: "Hôtels à insectes"
description: "Faire des coins pour que les insectes puissent y vivre"
kind: "page"

menu:
  main:
    parent: "projets-en-cours"

banner: "/images/projets-realises/hotels-a-insectes/banner.jpg"

tags:
  - "Insectes"
  - "Auxiliaires"
  - "Pollinisateurs"
---

Nous interventions dans le jardin étant limitées au maximum, les auxiliaires ont la possibilité de s'installer un peu partout.
Un espace spécialement aménagé avec des roseaux, bois troués, pives leur est dédié au sein de la [spirale]({{< ref "/projets-realises/spirale-aromatique" >}} "spirale aromatique").

{{< columns >}}
  {{< img
    src="/images/projets-realises/hotels-a-insectes/hotel-spirale.jpg"
    column-classes="is-half"
    figure-classes="is-4by3 m-0"
    alt="Roseaux pour les insectes"
    title="Des roseaux, du bois creux et des pives permettent aux insectes de se faire un abri dans la spirale aromatique."
  >}}
{{< /columns >}}

_Peu de locataires pour le moment, l'exposition sud n'étant pas 100% respectée et la hauteur au sol assez faible_
