module.exports = {
	plugins: [
		require("autoprefixer"),
		require("postcss-nested"),
		...(process.env.HUGO_ENVIRONMENT === "production"
			? [
					require("@fullhuman/postcss-purgecss")({
						content: ["layouts/**/*.html"],
					}),
					require("cssnano")({
						preset: "default",
					}),
			  ]
			: []),
	],
};
